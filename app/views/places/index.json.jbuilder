json.array!(@places) do |place|
  json.extract! place, :id, :nombre, :descripcion
  json.url place_url(place, format: :json)
end
