json.array!(@ubications) do |ubication|
  json.extract! ubication, :id, :latitud, :longitud
  json.url ubication_url(ubication, format: :json)
end
