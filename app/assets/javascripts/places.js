function geolocalizar(){
	if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(function(objPosition)
		{
			var lon = objPosition.coords.longitude;
			var lat = objPosition.coords.latitude;
			document.getElementById("place_ubication_attributes_latitud").value = lat;
			document.getElementById("place_ubication_attributes_longitud").value = lon;
			var dir = "";
			var latlng = new google.maps.LatLng(lat, lon);
			geocoder = new google.maps.Geocoder();
			geocoder.geocode({"latLng": latlng}, function(results, status)
			{
				if (status == google.maps.GeocoderStatus.OK)
				{
					if (results[0])
					{
						dir = "Dirección: " + results[0].formatted_address;
					}
					else
					{
						dir = "No se ha podido obtener ninguna dirección en esas coordenadas.";
					}
				}
				else
				{
					dir = "El Servicio de Codificación Geográfica ha fallado con el siguiente error: " + status;
				}

				alert("Latitud: " + lat + "Longitud: " + lon  + dir);
			});
		}, function(objPositionError)
		{
			switch (objPositionError.code)
			{
				case objPositionError.PERMISSION_DENIED:
					alert("No se ha permitido el acceso a la posición del usuario.");
				break;
				case objPositionError.POSITION_UNAVAILABLE:
					alert("No se ha podido acceder a la información de su posición.");
				break;
				case objPositionError.TIMEOUT:
					alert("El servicio ha tardado demasiado tiempo en responder.");
				break;
				default:
					alert("Error desconocido.");
			}
		}, {
			maximumAge: 75000,
			timeout: 15000
		});
	}
	else
	{
		alert("Su navegador no soporta la API de geolocalización.");
	}
}