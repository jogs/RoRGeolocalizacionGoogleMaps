class Place < ActiveRecord::Base
	has_one :ubication
	accepts_nested_attributes_for :ubication, allow_destroy: true
  
  	validates :nombre, presence: { message: ": es un campo obligatorio"}

end
