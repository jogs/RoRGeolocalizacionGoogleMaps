class CreateUbications < ActiveRecord::Migration
  def change
    create_table :ubications do |t|
      t.string :latitud, limit: 22
      t.string :longitud, limit: 22

      t.timestamps null: false
    end
  end
end
