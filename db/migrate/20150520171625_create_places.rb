class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :nombre, limit: 40
      t.string :descripcion, limit: 144

      t.timestamps null: false
    end
  end
end
