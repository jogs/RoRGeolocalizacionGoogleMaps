class AddPlaceIdToUbications < ActiveRecord::Migration
  def change
    add_reference :ubications, :place, index: true
    add_foreign_key :ubications, :places
  end
end
